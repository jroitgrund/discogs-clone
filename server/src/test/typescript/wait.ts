import { Timeouts } from "../../main/typescript/Timeouts";

const timer = new Timeouts(global as any);

export function wait() {
  return timer.delay(0);
}
