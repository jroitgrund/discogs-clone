import lolex, { Clock } from "lolex";

import { Throttler } from "../../main/typescript/Throttler";
import { Timeouts } from "../../main/typescript/Timeouts";

import { wait } from "./wait";

const MAX_REQS_PER_INTERVAL = 5;
const INTERVAL = 10;
const TIME_BETWEEN_REQS = Math.ceil(INTERVAL / MAX_REQS_PER_INTERVAL);

describe("Throttler", () => {
  let clock: Clock;
  let throttler: Throttler;
  let requestsTotal: number;
  let time: number;
  let request: () => Promise<number>;

  beforeEach(() => {
    clock = lolex.createClock();
    throttler = new Throttler(
      new Timeouts(clock),
      MAX_REQS_PER_INTERVAL,
      INTERVAL,
    );
    requestsTotal = 0;
    time = 0;
    request = throttler.throttle(async () => requestsTotal++);
  });

  async function tick() {
    clock.tick(1);
    time++;
    await wait();
  }

  function requestN(n: number) {
    for (let i = 0; i < n; i++) {
      request();
    }
  }

  function expectRequests(n: number) {
    expect(requestsTotal).toEqual(n);
  }

  test("throttles", async () => {
    requestN(INTERVAL * 10 * MAX_REQS_PER_INTERVAL);
    await wait();

    while (time < INTERVAL * 10) {
      const allowedRequests = Math.floor(time / TIME_BETWEEN_REQS) + 1;
      expectRequests(allowedRequests);
      await tick();
    }
  });
});
