import { includes } from "lodash";
import redis from "redis";
import { promisify } from "util";

import { Discogs } from "./Discogs";
import { Queue } from "./Queue";

export enum FetchStatus {
  UNKNOWN,
  QUEUED,
  IN_PROGRESS,
  DONE,
}

enum FetchType {
  LABEL,
  ARTIST,
}

const ARTIST_SET_KEY = "fetched-artists";
const LABEL_SET_KEY = "fetched-labels";

export class InProgress {
  private artistIdQueue: number[] = [];
  private labelIdQueue: number[] = [];
  private currentlyFetching: number | null = null;
  private currentlyFetchingType: FetchType | null = null;

  private sismember: any;
  private sadd: any;

  constructor(
    redisClient: redis.RedisClient,
    readonly discogs: Discogs,
    readonly queue: Queue,
  ) {
    this.sismember = promisify(redisClient.sismember).bind(redisClient);
    this.sadd = promisify(redisClient.sadd).bind(redisClient);
  }

  public async getLabelStatus(id: number): Promise<FetchStatus> {
    if (await this.sismember(LABEL_SET_KEY, id)) {
      return FetchStatus.DONE;
    } else if (
      this.currentlyFetching === id &&
      this.currentlyFetchingType === FetchType.LABEL
    ) {
      return FetchStatus.IN_PROGRESS;
    } else if (includes(this.labelIdQueue, id)) {
      return FetchStatus.QUEUED;
    } else {
      return FetchStatus.UNKNOWN;
    }
  }

  public async getArtistStatus(id: number): Promise<FetchStatus> {
    if (await this.sismember(ARTIST_SET_KEY, id)) {
      return FetchStatus.DONE;
    } else if (
      this.currentlyFetching === id &&
      this.currentlyFetchingType === FetchType.ARTIST
    ) {
      return FetchStatus.IN_PROGRESS;
    } else if (includes(this.artistIdQueue, id)) {
      return FetchStatus.QUEUED;
    } else {
      return FetchStatus.UNKNOWN;
    }
  }

  public triggerLabelFetch(id: number) {
    this.labelIdQueue.push(id);
    this.queue.addTask(this.fetchLabel);
  }

  public triggerArtistFetch(id: number) {
    this.artistIdQueue.push(id);
    this.queue.addTask(this.fetchArtist);
  }

  private fetchArtist = async () => {
    this.currentlyFetching = this.artistIdQueue.shift() as number;
    this.currentlyFetchingType = FetchType.ARTIST;
    await this.discogs.cacheFetchesForArtist(this.currentlyFetching);
    await this.sadd(ARTIST_SET_KEY, this.currentlyFetching);
  }

  private fetchLabel = async () => {
    this.currentlyFetching = this.labelIdQueue.shift() as number;
    this.currentlyFetchingType = FetchType.LABEL;
    await this.discogs.cacheFetchesForLabel(this.currentlyFetching);
    await this.sadd(LABEL_SET_KEY, this.currentlyFetching);
  }
}
