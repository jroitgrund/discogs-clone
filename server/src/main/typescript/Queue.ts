import { logger } from "./logger";

export class Queue {
  private busy: boolean = false;
  private tasks: Array<() => any> = [];

  public addTask(action: () => any) {
    this.tasks.push(action);
    if (!this.busy) {
      setImmediate(() => this.startRunner());
    }
  }

  private async startRunner() {
    this.busy = true;
    while (this.tasks.length > 0) {
      const action = this.tasks.shift() as () => any;
      try {
        await action();
      } catch (e) {
        logger.error(e.stack);
      }
    }
    this.busy = false;
  }
}
