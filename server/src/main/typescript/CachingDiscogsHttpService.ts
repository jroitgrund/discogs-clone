import { Request, RequestInit, Response } from "node-fetch";
import redis from "redis";
import { promisify } from "util";

export class CachingDiscogsHttpService {
  private exists: any;
  private set: any;
  private get: any;
  private localPromises: { [key: string]: Promise<any> } = {};

  constructor(
    client: redis.RedisClient,
    readonly fetch: (
      url: string | Request,
      init?: RequestInit,
    ) => Promise<Response>,
    readonly userToken: string,
  ) {
    this.exists = promisify(client.exists).bind(client);
    this.set = promisify(client.set).bind(client);
    this.get = promisify(client.get).bind(client);
  }

  public async queryUrl(url: uri.URI): Promise<any> {
    const urlString = url.valueOf();
    if (await this.exists(urlString)) {
      return JSON.parse(await this.get(urlString));
    } else if (this.localPromises[urlString]) {
      return this.localPromises[urlString];
    } else {
      const resultPromise = this.queryDiscogs(url);
      this.localPromises[urlString] = resultPromise;
      const result = await resultPromise;
      await this.set(urlString, JSON.stringify(result));
      delete this.localPromises[urlString];
      return result;
    }
  }

  private async queryDiscogs(url: uri.URI): Promise<any> {
    const urlWithKey = url.addQuery({ token: this.userToken });
    const response = await this.fetch(urlWithKey.valueOf(), {
      headers: {
        "user-agent":
          "DiscogsCloner/0.0.1 +https://gitlab.com/jroitgrund/discogs-cloner",
      },
    });
    const json: any = await response.json();
    if (json.message === "You are making requests too quickly.") {
      throw new Error("Made requests too quickly!");
    }
    return json;
  }
}
