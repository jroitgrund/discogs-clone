import { Timeouts } from "./Timeouts";

const MAX_REQS_PER_INTERVAL = 50;
const INTERVAL = 60_000;

export class Throttler {
  private nextReqRunnable = Promise.resolve();
  private readonly timeBetweenReqs: number;

  public constructor(
    readonly timeouts: Timeouts,
    maxReqsPerInterval = MAX_REQS_PER_INTERVAL,
    interval = INTERVAL,
  ) {
    this.timeBetweenReqs = Math.ceil(interval / maxReqsPerInterval);
  }

  public throttle<F extends Function>(f: F): F {
    return ((...args: any[]) =>
      this.runThrottled(() => f.apply(undefined, args))) as any;
  }

  private async runThrottled<T>(action: () => Promise<T>): Promise<T> {
    await this.waitForNextReq();
    return action();
  }

  private async waitForNextReq() {
    const wait = this.nextReqRunnable;
    this.nextReqRunnable = wait.then(() =>
      this.timeouts.delay(this.timeBetweenReqs),
    );
    await wait;
  }
}
