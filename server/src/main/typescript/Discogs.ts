import _ from "lodash";
import URI from "urijs";

import { CachingDiscogsHttpService } from "./CachingDiscogsHttpService";
import { logger } from "./logger";

const BASE = URI("https://api.discogs.com");

interface IDiscogsArtist extends IDiscogsEntityWithReleaseUrl {
  resource_url: string;
  aliases?: IDiscogsArtistMetadata[];
  groups?: IDiscogsArtistMetadata[];
}

interface IDiscogsEntityWithReleaseUrl {
  id: number;
  name: string;
  releases_url: string;
}

interface IDiscogsMaster {
  title?: string;
  id?: number;
  versions_url: string;
  main_release_url: string;
}

interface IDiscogsArtistMetadata {
  id: number;
  resource_url: string;
  name: string;
}

interface IDiscogsRelease {
  artists: [{ name: string; id: number }];
  videos: [
    {
      uri: string;
      title: string;
    }
  ];
  title: string;
  id: number;
  labels: [{ id: number; resource_url: string; name: string }];
  uri: string;
  master_url?: string;
  community?: {
    rating?: { count?: number; average?: number };
    have?: number;
    want?: number;
  };
  formats: [{ descriptions: string[] }];
}

type ReleaseOrMaster = IDiscogsMaster & IDiscogsRelease;

interface ILabel {
  id: number;
  name: string;
}

export interface IRelease {
  videos: [
    {
      title: string;
      uri: string;
    }
  ];
  artistName: string;
  artistId: number;
  title: string;
  id: number;
  label: ILabel;
  ratings: number;
  rating: number;
  have: number;
  want: number;
}

export interface IEntityWithReleases {
  name: string;
  id: number;
  releases: IRelease[];
}

interface IVersion {
  resource_url: string;
}

export class Discogs {
  constructor(readonly cachingDiscogsHttpService: CachingDiscogsHttpService) {}

  public async cacheFetchesForArtist(id: number) {
    await this.getAllArtistReleases(id);
  }

  public async cacheFetchesForLabel(id: number) {
    await this.getAllLabelReleases(id);
  }

  public async getAllArtistReleases(id: number): Promise<IEntityWithReleases> {
    const mainAlias = await this.getArtist(id);
    const aliases: IDiscogsArtistMetadata[] = mainAlias.aliases || [];
    const groups: IDiscogsArtistMetadata[] = mainAlias.groups || [];
    const allAliases: IDiscogsArtist[] = [
      mainAlias,
      ...(await Promise.all(
        [...aliases, ...groups].map(({ resource_url }) =>
          this.queryUrl(URI(resource_url)),
        ),
      )),
    ];

    logger.silly(
      `Crawling aliases ${allAliases.map(alias => alias.name).join(", ")}`,
    );

    const allAliasesWithReleases = await Promise.all(
      allAliases.map(this.getEntityReleases),
    );

    return {
      id: mainAlias.id,
      name: mainAlias.name,
      releases: _.uniqBy(
        _.flatten(allAliasesWithReleases),
        release => release.id,
      ),
    };
  }

  public async getAllLabelReleases(id: number): Promise<IEntityWithReleases> {
    const label: IDiscogsEntityWithReleaseUrl = await this.getLabel(id);
    return {
      id: label.id,
      name: label.name,
      releases: await this.getEntityReleases(label),
    };
  }

  private getEntityReleases = async (
    entity: IDiscogsEntityWithReleaseUrl,
  ): Promise<IRelease[]> => {
    const { name, releases_url } = entity;
    const discogsReleases = await this.getReleaseList(releases_url);
    logger.silly(`Found ${discogsReleases.length} releases for artist ${name}`);
    let numProcessed = 0;
    const releases = _.compact(
      await Promise.all(
        discogsReleases.map(async release => {
          const releaseDetails = await this.getReleaseDetails(release);
          logger.silly(
            `Processed ${++numProcessed} / ${discogsReleases.length} releases`,
          );
          return releaseDetails;
        }),
      ),
    );
    return _.uniqBy(releases, release => release.id);
  }

  private getArtist(id: number): IDiscogsArtist {
    return this.queryPath("artists", String(id)) as any;
  }

  private getLabel(id: number): IDiscogsEntityWithReleaseUrl {
    return this.queryPath("labels", String(id)) as any;
  }

  private async getReleaseList(
    releasesUrl: string,
  ): Promise<ReleaseOrMaster[]> {
    const releaseSummaries = ((await this.queryUrlPaginated(
      releasesUrl,
      "releases",
    )) as any) as Array<{ resource_url: string }>;
    return Promise.all(
      releaseSummaries.map(({ resource_url }) =>
        this.queryUrl(URI(resource_url)),
      ),
    ) as any;
  }

  private getReleaseDetails = async (
    release: ReleaseOrMaster,
  ): Promise<IRelease | null> => {
    logger.silly(`Processing ${release.title} (${release.id})`);
    if (release.versions_url) {
      logger.silly(`It's a master`);
      return this.aggregateReleasesFromMaster(release as IDiscogsMaster);
    } else if (release.master_url) {
      logger.silly(`It has a master, fetching it`);
      const master: IDiscogsMaster = await this.queryUrl(
        URI(release.master_url),
      );
      return this.aggregateReleasesFromMaster(master);
    } else {
      logger.silly(`It has no master, returning as is`);
      return toRelease(release as IDiscogsRelease);
    }
  }

  private async aggregateReleasesFromMaster(
    master: IDiscogsMaster,
  ): Promise<IRelease | null> {
    const main: IDiscogsRelease = await this.queryUrl(
      URI(master.main_release_url),
    );
    if (!isValidFormat(main)) {
      return null;
    }
    logger.silly(`Processing master ${master.title} (${master.id})`);
    const versions = (await this.queryUrlPaginated(
      master.versions_url,
      "versions",
    )) as IVersion[];
    logger.silly(
      `Found ${versions.length} versions: ${JSON.stringify(
        versions,
        null,
        "  ",
      )}`,
    );
    const discogsReleases: IDiscogsRelease[] = await Promise.all(
      versions.map(
        ({ resource_url }) =>
          this.queryUrl(URI(resource_url)) as Promise<IDiscogsRelease>,
      ),
    );
    logger.silly(
      `Mapped ${discogsReleases.length} versions: ${JSON.stringify(
        discogsReleases,
        null,
        "  ",
      )}`,
    );
    const releases = _.compact(discogsReleases.map(toRelease));
    const rating = releases.reduce(
      (total, release) => ({
        have: total.have + release.have,
        ratings: total.ratings + release.ratings,
        rating: total.rating + release.ratings * release.rating,
        want: total.want + release.want,
      }),
      { ratings: 0, rating: 0, have: 0, want: 0 },
    );

    return {
      videos: main.videos,
      artistName: main.artists[0].name,
      artistId: main.artists[0].id,
      have: rating.have,
      want: rating.want,
      ratings: rating.ratings,
      rating: rating.ratings > 0 ? rating.rating / rating.ratings : 0,
      id: main.id,
      title: main.title,
      label: {
        id: main.labels[0].id,
        name: main.labels[0].name,
      },
    };
  }

  private async queryUrlPaginated(url: string, key: string): Promise<any[]> {
    const urlWithPagination = URI(url).addQuery({ per_page: 100 });
    let page = {
      pagination: {
        urls: {
          next: urlWithPagination.valueOf(),
        },
      },
      [key]: [],
    };
    const items: any = [];

    while (
      page.pagination &&
      page.pagination.urls &&
      page.pagination.urls.next
    ) {
      page = await this.queryUrl(URI(page.pagination.urls.next));
      items.push(...(page[key] as any[]));
    }

    return items;
  }

  private async queryPath(...pathComponents: string[]): Promise<any> {
    const url = this.buildUrl(...pathComponents);
    return this.queryUrl(url);
  }

  private async queryUrl(url: uri.URI): Promise<any> {
    return this.cachingDiscogsHttpService.queryUrl(url);
  }

  private buildUrl(...pathComponents: string[]): uri.URI {
    return BASE.clone().path(pathComponents.join("/"));
  }
}

function toRelease(discogsRelease: IDiscogsRelease): IRelease | null {
  if (!isValidFormat(discogsRelease)) {
    return null;
  }
  logger.silly(
    `Converting standalone release ${discogsRelease.title} (${
      discogsRelease.id
    })`,
  );
  const want = (discogsRelease.community && discogsRelease.community.want) || 0;
  const have = (discogsRelease.community && discogsRelease.community.have) || 0;
  const ratings =
    (discogsRelease.community &&
      discogsRelease.community.rating &&
      discogsRelease.community.rating.count) ||
    0;
  const rating =
    (discogsRelease.community &&
      discogsRelease.community.rating &&
      discogsRelease.community.rating.average) ||
    0;

  return {
    videos: discogsRelease.videos,
    artistName: discogsRelease.artists[0].name,
    artistId: discogsRelease.artists[0].id,
    title: discogsRelease.title,
    id: discogsRelease.id,
    label: {
      id: discogsRelease.labels[0].id,
      name: discogsRelease.labels[0].name,
    },
    want,
    have,
    ratings,
    rating,
  };
}

function isValidFormat(release: IDiscogsRelease) {
  const allFormats = _.flatMap(
    release.formats.map(format => format.descriptions),
  );
  return !(
    _.includes(allFormats, "Compilation") || _.includes(allFormats, "Mixed")
  );
}
