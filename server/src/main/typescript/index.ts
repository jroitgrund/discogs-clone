import express from "express";
import helmet from "helmet";
import morgan from "morgan";
import _fetch from "node-fetch";
import path from "path";
import redis from "redis";

import { CachingDiscogsHttpService } from "./CachingDiscogsHttpService";
import { Discogs } from "./Discogs";
import "./env";
import { FetchStatus, InProgress } from "./InProgress";
import { logger } from "./logger";
import { Queue } from "./Queue";
import { Throttler } from "./Throttler";
import { Timeouts } from "./Timeouts";

const timeouts = new Timeouts(global as any);
const throttler = new Throttler(timeouts);
const fetch = throttler.throttle(_fetch);
const redisClient = redis.createClient();
const cachingDiscogsHttpService = new CachingDiscogsHttpService(
  redisClient,
  fetch,
  process.env.DISCOGS_USER_TOKEN as string,
);
const discogs = new Discogs(cachingDiscogsHttpService);
const inProgress = new InProgress(redisClient, discogs, new Queue());

const contextPath = process.env.CONTEXT_PATH || "/";

const app = express();
app.use(
  helmet({
    hsts: false,
    referrerPolicy: {
      policy: "no-referrer",
    },
  }),
);
app.use(morgan("common"));
app.use(
  `${contextPath}static`,
  express.static(path.join(__dirname, "/../../../../client/dist"), {
    immutable: true,
    maxAge: 31536000000,
  }),
);
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "/../../../../client/dist"));

app.get(`${contextPath}`, (req, res) => {
  res.render("index", {
    contextPath,
  });
});

app.get(`${contextPath}api/labels/:labelId/status`, async (req, res) => {
  res.json(await inProgress.getLabelStatus(req.params.labelId));
});

app.get(`${contextPath}api/artists/:artistId/status`, async (req, res) => {
  res.json(await inProgress.getArtistStatus(req.params.artistId));
});

app.get(`${contextPath}api/labels/:labelId`, async (req, res) => {
  const { labelId } = req.params;
  const status = await inProgress.getLabelStatus(labelId);

  if (status !== FetchStatus.DONE) {
    res.status(404).send();
    return;
  }
  res.json(await discogs.getAllLabelReleases(labelId));
});

app.get(`${contextPath}api/artists/:artistId`, async (req, res) => {
  const { artistId } = req.params;
  const status = await inProgress.getArtistStatus(artistId);

  if (status !== FetchStatus.DONE) {
    res.status(404).send();
    return;
  }
  res.json(await discogs.getAllArtistReleases(artistId));
});

app.post(`${contextPath}api/labels/:labelId`, async (req, res) => {
  await inProgress.triggerLabelFetch(req.params.labelId);
  res.status(200).send();
});

app.post(`${contextPath}api/artists/:artistId`, async (req, res) => {
  await inProgress.triggerArtistFetch(req.params.artistId);
  res.status(200).send();
});

app.listen(3000, () =>
  logger.info(`discogs-clone listening on localhost:3000${contextPath}!`),
);
