import { Clock } from "lolex";

export class Timeouts {
  constructor(readonly clock: Clock) {}

  public setTimeoutPromise<T>(action: () => T, timeoutMs: number): Promise<T> {
    return new Promise((resolve, reject) => {
      this.clock.setTimeout(() => {
        try {
          resolve(action());
        } catch (e) {
          reject(e);
        }
      }, timeoutMs);
    });
  }

  public delay(timeoutMs: number): Promise<void> {
    return this.setTimeoutPromise(() => {
      /* do nothing */
    }, timeoutMs);
  }
}
