import { Container } from "unstated";

import {
  fetchArtist,
  fetchLabel,
  getArtist,
  getArtistStatus,
  getLabel,
  getLabelStatus,
  IEntityWithReleases,
} from "./http";

export enum Type {
  ARTIST,
  LABEL,
}

export enum FetchStatus {
  UNKNOWN,
  QUEUED,
  IN_PROGRESS,
  DONE,
}

export interface IYoutubeVideo {
  title: string;
  uri: string;
}

export interface IAppState {
  videos: IYoutubeVideo[];
  index: number;
  status: FetchStatus | undefined;
  entity: IEntityWithReleases | undefined;
  lastId: string | undefined;
  lastType: Type | undefined;
}

export class AppStateContainer extends Container<IAppState> {
  public state = {
    videos: [],
    index: -1,
    status: undefined,
    entity: undefined,
    lastId: undefined,
    lastType: undefined,
  } as IAppState;

  public fetch = async () => {
    const fetch =
      this.state.lastType === Type.ARTIST ? fetchArtist : fetchLabel;
    await fetch(this.state.lastId as string);
    this.refresh();
  }

  public refresh = () => {
    this.getStatus(this.state.lastId as string, this.state.lastType as Type);
  }

  public getStatus = (id: string, type: Type) => {
    const getStatus = type === Type.ARTIST ? getArtistStatus : getLabelStatus;
    const get = type === Type.ARTIST ? getArtist : getLabel;
    this.setState(
      {
        status: undefined,
        entity: undefined,
        lastId: id,
        lastType: type,
      },
      async () => {
        const status = await getStatus(id);
        const entity = status === FetchStatus.DONE ? await get(id) : undefined;
        this.setState({
          status,
          entity,
        });
      },
    );
  }

  public addVideo = (...video: IYoutubeVideo[]) => {
    this.setState({
      videos: [...this.state.videos, ...video],
      index: this.state.index === -1 ? 0 : this.state.index,
    });
  }

  public playIndex = (index: number) => {
    this.setState({
      index,
    });
  }

  public nextVideo = () => {
    if (this.state.index < this.state.videos.length - 1) {
      this.setState({
        index: this.state.index + 1,
      });
    }
  }
}

export const appState = new AppStateContainer();
