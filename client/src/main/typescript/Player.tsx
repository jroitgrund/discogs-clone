import React from "react";
import { Subscribe } from "unstated";

import { appState, AppStateContainer, IAppState, IYoutubeVideo } from "./state";

const URL = /^https:\/\/www.youtube.com\/watch\?v=([^&?]+)/;

function getId(url: string) {
  return (url.match(URL) as RegExpMatchArray)[1];
}

export class Player extends React.PureComponent<{}, {}> {
  public render() {
    return (
      <Subscribe to={[AppStateContainer]}>
        {(container: AppStateContainer) => (
          <div>
            <Youtube playlist={container.state} />
            {container.state.videos.map((video, index) => (
              <ClickableLink
                video={video}
                key={index}
                index={index}
                isCurrent={container.state.index === index}
              />
            ))}
          </div>
        )}
      </Subscribe>
    );
  }
}

class ClickableLink extends React.PureComponent<
  { video: IYoutubeVideo; index: number; isCurrent: boolean },
  {}
> {
  public render() {
    return (
      <div>
        <a
          style={this.props.isCurrent ? { fontWeight: "bold" } : {}}
          onClick={this.setIndex}
        >
          {this.props.video.title}
        </a>
      </div>
    );
  }

  private setIndex = () => {
    appState.playIndex(this.props.index);
  }
}

class Youtube extends React.Component<{ playlist: IAppState }, {}> {
  private player: any;
  private previousIndex = -1;

  public constructor(props: { playlist: IAppState }) {
    super(props);
  }

  public shouldComponentUpdate(props: { playlist: IAppState }) {
    if (this.previousIndex === props.playlist.index) {
      return false;
    }

    this.previousIndex = props.playlist.index;

    if (props.playlist.index < 0) {
      return false;
    }

    const videoId = getId(props.playlist.videos[props.playlist.index].uri);

    if (!this.player) {
      return (
        new (window as any).YT.Player("player", {
          height: "320",
          width: "480",
          videoId,
          events: {
            onReady: this.onPlayerReady,
            onStateChange: this.onPlayerStateChange,
          },
        }) && false
      );
    } else {
      this.player.loadVideoById(videoId);
      this.player.playVideo();
    }

    return false;
  }

  private onPlayerReady = (event: { target: any }) => {
    this.player = event.target;
    this.player.playVideo();
  }

  private onPlayerStateChange = (event: { data: number }) => {
    if (event.data === 0) {
      appState.nextVideo();
    }
  }

  public render() {
    return <div id="player" />;
  }
}
