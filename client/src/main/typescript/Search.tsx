import React, { FormEvent } from "react";

import { appState, Type } from "./state";
import { Status } from "./Status";

interface ISearchState {
  idField: string;
  selectValue: string;
}

export class Search extends React.PureComponent<{}, ISearchState> {
  public state: ISearchState = {
    idField: "",
    selectValue: "Artist",
  };

  public render() {
    return (
      <div>
        <form className="pure-form">
          <fieldset>
            <input type="text" placeholder="id" onChange={this.setIdField} />{" "}
            <select onChange={this.setSelectValue}>
              <option value="Artist">Artist</option>
              <option value="Label">Label</option>
            </select>{" "}
            <button type="button" className="pure-button" onClick={this.go}>
              GO
            </button>
          </fieldset>
        </form>
        <Status />
      </div>
    );
  }

  private go = () => {
    appState.getStatus(
      this.state.idField,
      this.state.selectValue === "Artist" ? Type.ARTIST : Type.LABEL,
    );
  }

  private setIdField = (e: FormEvent) => {
    this.setState({
      idField: (e.target as HTMLInputElement).value,
    });
  }

  private setSelectValue = (e: FormEvent) => {
    this.setState({
      selectValue: (e.target as HTMLInputElement).value,
    });
  }
}
