import React from "react";

import { Subscribe } from "../../../../node_modules/unstated";

import { ReleaseList } from "./ReleaseList";

import { appState, AppStateContainer, FetchStatus } from "./state";

export class Status extends React.PureComponent<{}, {}> {
  public render() {
    return (
      <Subscribe to={[AppStateContainer]}>
        {(container: AppStateContainer) => {
          if (!container.state.status === undefined) {
            return null;
          }
          if (container.state.status === FetchStatus.DONE) {
            return <ReleaseList />;
          } else if (
            container.state.status === FetchStatus.QUEUED ||
            container.state.status === FetchStatus.IN_PROGRESS
          ) {
            return "Queued for fetching. Check back soon!";
          } else if (container.state.status === FetchStatus.UNKNOWN) {
            return (
              <div>
                Unknown{" "}
                <button type="button" onClick={appState.fetch}>
                  Fetch
                </button>
              </div>
            );
          } else {
            return null;
          }
        }}
      </Subscribe>
    );
  }
}
