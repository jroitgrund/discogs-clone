import React from "react";
import { hot } from "react-hot-loader";
import { cssRule, style } from "typestyle";
import { Provider } from "unstated";

import { Player } from "./Player";
import { Search } from "./Search";
import { appState } from "./state";

cssRule("body", {
  fontFamily:
    '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, ' +
    '"Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
});

cssRule("a, a:link, a:visited, a:hover, a:active", {
  color: "black",
});

const TWO_COLUMNS = style({
  display: "flex",
  width: "1280px",
  justifyContent: "center",
  margin: "10px",
});

const PLAYER = style({
  width: "480px",
});

const MAIN = style({
  width: "800px",
  marginLeft: "10px",
});

const App = () => (
  <Provider inject={[appState]}>
    <div className={TWO_COLUMNS}>
      <div className={PLAYER}>
        <Player />
      </div>
      <div className={MAIN}>
        <Search />
      </div>
    </div>
  </Provider>
);

export default hot(module)(App);
