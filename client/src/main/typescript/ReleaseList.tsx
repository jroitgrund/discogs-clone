import _ from "lodash";
import React from "react";
import { Subscribe } from "unstated";

import { IRelease } from "./http";
import { appState, AppStateContainer } from "./state";

export class ReleaseList extends React.PureComponent<{}, {}> {
  public render() {
    return (
      <Subscribe to={[AppStateContainer]}>
        {(container: AppStateContainer) => {
          if (!container.state.entity) {
            return null;
          }

          const releasesByRating = _.sortBy(
            container.state.entity.releases,
            release => -release.rating,
          );
          return (
            <div>
              <a href={artistUrl(container.state.entity.id)}>
                <h1>{container.state.entity.name}</h1>
              </a>
              <ul>
                {releasesByRating.map((release, i) => (
                  <ClickableRelease key={i} release={release} />
                ))}
              </ul>
            </div>
          );
        }}
      </Subscribe>
    );
  }
}

class ClickableRelease extends React.PureComponent<{ release: IRelease }, {}> {
  public render() {
    return (
      <li>
        <a href={releaseUrl(this.props.release.id)}>
          {this.props.release.title}
        </a>{" "}
        ({this.props.release.rating}, as{" "}
        <a href={artistUrl(this.props.release.artistId)}>
          {this.props.release.artistName}
        </a>, on{" "}
        <a href={labelUrl(this.props.release.label.id)}>
          {this.props.release.label.name}
        </a>)
        {this.props.release.videos && this.props.release.videos.length > 0 ? (
          <button onClick={this.loadVideo}>Play</button>
        ) : null}
      </li>
    );
  }

  public loadVideo = () => {
    appState.addVideo(...this.props.release.videos);
  }
}

function artistUrl(id: number) {
  return `https://www.discogs.com/artist/${id}`;
}

function releaseUrl(id: number) {
  return `https://www.discogs.com/release/${id}`;
}

function labelUrl(id: number) {
  return `https://www.discogs.com/label/${id}`;
}
