import { FetchStatus } from "./state";

export interface IEntityWithReleases {
  name: string;
  id: number;
  releases: IRelease[];
}

export interface IRelease {
  videos: [
    {
      title: string;
      uri: string;
    }
  ];
  artistName: string;
  artistId: number;
  title: string;
  id: number;
  label: ILabel;
  ratings: number;
  rating: number;
  have: number;
  want: number;
}

interface ILabel {
  id: number;
  name: string;
}

export async function getArtist(id: string): Promise<IEntityWithReleases> {
  return (await fetch(`api/artists/${id}`)).json();
}

export async function getLabel(id: string): Promise<IEntityWithReleases> {
  return (await fetch(`api/labels/${id}`)).json();
}

export async function getArtistStatus(id: string): Promise<FetchStatus> {
  return (await fetch(`api/artists/${id}/status`)).json();
}

export async function getLabelStatus(id: string): Promise<FetchStatus> {
  return (await fetch(`api/labels/${id}/status`)).json();
}

export async function fetchArtist(id: string): Promise<FetchStatus> {
  return (await fetch(`api/artists/${id}`, { method: "post" })).status;
}

export async function fetchLabel(id: string): Promise<FetchStatus> {
  return (await fetch(`api/labels/${id}`, { method: "post" })).status;
}
