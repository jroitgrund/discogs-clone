const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlWebpackHarddiskPlugin = require("html-webpack-harddisk-plugin");
const path = require("path");

module.exports = (env, argv) => ({
  entry: {
    index: "./client/src/main/typescript/index.tsx",
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].js",
    publicPath:
      argv.mode === "production" ? "static/" : "http://localhost:8080/",
  },
  devtool: argv.mode === "production" ? undefined : "source-map",
  devServer: {
    publicPath: "http://localhost:8080/",
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  },
  resolve: {
    extensions: [".tsx", ".js", ".ts", ".jsx"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: (argv.mode === "production"
          ? []
          : [
              {
                loader: "babel-loader",
                options: {
                  babelrc: false,
                  plugins: ["react-hot-loader/babel"],
                },
              },
            ]
        ).concat([
          {
            loader: "awesome-typescript-loader",
            options: {
              configFileName: path.resolve(
                __dirname,
                argv.mode === "production"
                  ? "tsconfig.json"
                  : "tsconfig.dev.json",
              ),
            },
          },
        ]),
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./client/src/main/html/index.html",
      filename: "index.hbs",
      alwaysWriteToDisk: true,
    }),
    new HtmlWebpackHarddiskPlugin(),
  ],
});
